<?php
/**
 * Title: Section: Testimonials (text).
 * Slug: wpct/general-testimonials
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"layout":{"inherit":true}} -->
<div class="wp-block-group alignfull" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:heading {"level":4,"style":{"typography":{"fontSize":"var(--wp--preset--font-size--x-large-sxl)","lineHeight":"var(--wp--custom--line-height--one)"}}} -->
<h4 style="font-size:var(--wp--preset--font-size--x-large-sxl);line-height:var(--wp--custom--line-height--one);">“</h4>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae lorem a neque imperdiet sagittis. Vivamus enim velit.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size"><strong>—Allison Taylor, Designer</strong></p>
<!-- /wp:paragraph --></div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:heading {"level":4,"style":{"typography":{"fontSize":"var(--wp--preset--font-size--x-large-sxl)","lineHeight":"var(--wp--custom--line-height--one)"}}} -->
<h4 style="font-size:var(--wp--preset--font-size--x-large-sxl);line-height:var(--wp--custom--line-height--one)">“</h4>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Fusce at est sapien. Aliquam tempus et nulla nisipt rhoncus, morbi convallis magna swift. Morbi viverra lobortis ante, volutpat ipsum.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size"><strong>—Anthony Breck, Developer</strong></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:heading {"level":4,"style":{"typography":{"fontSize":"var(--wp--preset--font-size--x-large-sxl)","lineHeight":"var(--wp--custom--line-height--one)"}}} -->
<h4 style="font-size:var(--wp--preset--font-size--x-large-sxl);line-height:var(--wp--custom--line-height--one)">“</h4>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Quisque ullamcorper nulla breu elementum, atipo consectetur ex iaculis quis. Vestibulum et faucibus. Quisque vitae mi pellentesque.</p>
<!-- /wp:paragraph -->
<!-- wp:paragraph {"fontSize":"small"} -->
<p class="has-small-font-size"><strong>—Rebecca Jones, Coach</strong></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
