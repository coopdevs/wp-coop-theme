<?php
/**
 * Title: Header: Default (site title, navigation).
 * Slug: wpct/header-default
 * Categories: wpct-header
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sl)","bottom":"var(--wp--custom--spacing--sl)"},"margin":{"top":"0px"}}},"layout":{"inherit":true}} -->
<div class="wp-block-group alignfull" style="margin-top:0px;padding-top:var(--wp--custom--spacing--sl);padding-bottom:var(--wp--custom--spacing--sl)">
<!-- wp:group {"align":"wide","layout":{"type":"flex","justifyContent":"space-between"}} -->
<div class="wp-block-group alignwide">
<!-- wp:site-title {"style":{"elements":{"link":{"color":{"text":"var:preset|color|brand"}}}}} /-->
<!-- wp:navigation {"isResponsive":true,"style":{"spacing":{"blockGap":"var(--wp--custom--spacing--sxs)"}}} -->
<!-- wp:page-list {"isNavigationChild":true,"showSubmenuIcon":true,"openSubmenusOnClick":false} /-->
<!-- /wp:navigation -->
</div>
<!-- /wp:group -->
</div>
<!-- /wp:group -->
