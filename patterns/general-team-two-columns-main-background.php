<?php
/**
 * Title: Section: Team 2-columns background-color-main (image, text, link).
 * Slug: wpct/general-team-two-columns-main-background
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"elements":{"link":{"color":{"text":"var:preset|color|main-contrast"}}},"spacing":{"margin":{"top":"0px"}}},"backgroundColor":"main","textColor":"main-contrast","layout":{"inherit":true}} -->
<div class="wp-block-group alignfull has-main-contrast-color has-main-background-color has-text-color has-background has-link-color" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:group {"align":"wide"} -->
<div class="wp-block-group alignwide">
<!-- wp:heading {"fontSize":"x-large"} -->
<h2 class="has-x-large-font-size" id="our-team"><?php echo esc_html__( 'Our Team', 'wpct' ); ?></h2>
<!-- /wp:heading -->
<!-- wp:paragraph -->
<p><?php echo esc_html__( 'The people who are ready to serve you.', 'wpct' ); ?></p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:group -->
<!-- wp:spacer {"height":60} -->
<div style="height:60px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"align":"wide"} -->
<div class="wp-block-columns alignwide">
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sl)"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--custom--spacing--sl)">
<!-- wp:column {"width":"22%"} -->
<div class="wp-block-column" style="flex-basis:22%">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%">
<!-- wp:heading {"level":3,"fontSize":"medium"} -->
<h3 class="has-medium-font-size" id="member-name"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non et aliquam volutpat. <a href="#"><?php echo esc_html__( 'Read more', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sl)"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--custom--spacing--sl)"><!-- wp:column {"width":"22%"} -->
<div class="wp-block-column" style="flex-basis:22%"><!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%">
<!-- wp:heading {"level":3,"fontSize":"medium"} -->
<h3 class="has-medium-font-size" id="member-name"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non et aliquam volutpat. <a href="#"><?php echo esc_html__( 'Read more', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sl)"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--custom--spacing--sl)">
<!-- wp:column {"width":"22%"} -->
<div class="wp-block-column" style="flex-basis:22%">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%">
<!-- wp:heading {"level":3,"fontSize":"medium"} -->
<h3 class="has-medium-font-size" id="member-name-1"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->

<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non et aliquam volutpat. <a href="#"><?php echo esc_html__( 'Read more', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
</div>
<!-- /wp:column -->
<!-- wp:column -->
<div class="wp-block-column">
<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sl)"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--custom--spacing--sl)">
<!-- wp:column {"width":"22%"} -->
<div class="wp-block-column" style="flex-basis:22%">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%">
<!-- wp:heading {"level":3,"fontSize":"medium"} -->
<h3 class="has-medium-font-size" id="member-name-2"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non et aliquam volutpat. <a href="#"><?php echo esc_html__( 'Read more', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sl)"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--custom--spacing--sl)">
<!-- wp:column {"width":"22%"} -->
<div class="wp-block-column" style="flex-basis:22%">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%">
<!-- wp:heading {"level":3,"fontSize":"medium"} -->
<h3 class="has-medium-font-size" id="member-name"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non et aliquam volutpat. <a href="#"><?php echo esc_html__( 'Read more', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:columns {"style":{"spacing":{"margin":{"bottom":"var(--wp--custom--spacing--sl)"}}}} -->
<div class="wp-block-columns" style="margin-bottom:var(--wp--custom--spacing--sl)">
<!-- wp:column {"width":"22%"} -->
<div class="wp-block-column" style="flex-basis:22%">
<!-- wp:image {"id":3488,"sizeSlug":"full","linkDestination":"none"} -->
<figure class="wp-block-image size-full"><img src="<?php echo esc_url( __( 'https://coopdevs.org/wp-content/uploads/2022/05/it-intercooperacio.jpg', 'wpct' ) ); ?>"  alt="<?php echo esc_attr__( 'Sample Image', 'wpct' ); ?>" class="wp-image-3488"/></figure>
<!-- /wp:image -->
</div>
<!-- /wp:column -->
<!-- wp:column {"width":"75%"} -->
<div class="wp-block-column" style="flex-basis:75%">
<!-- wp:heading {"level":3,"fontSize":"medium"} -->
<h3 class="has-medium-font-size" id="member-name"><?php echo esc_html__( 'Member Name', 'wpct' ); ?></h3>
<!-- /wp:heading -->
<!-- wp:paragraph {"style":{"typography":{"fontSize":"var(--wp--preset--font-size--small)"}}} -->
<p style="font-size:var(--wp--preset--font-size--small)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non et aliquam volutpat. <a href="#"><?php echo esc_html__( 'Read more', 'wpct' ); ?></a>.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":60} -->
<div style="height:60px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
