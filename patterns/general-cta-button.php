<?php
/**
 * Title: Section: Call-to-action-button (text, button).
 * Slug: wpct/general-cta-button
 * Categories: wpct-general
 * Viewport Width: 1280
 */

?>
<!-- wp:group {"align":"full","style":{"spacing":{"margin":{"top":"0px"}}},"layout":{"inherit":true}} -->
<div class="wp-block-group alignfull" style="margin-top:0px">
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
<!-- wp:columns {"verticalAlignment":"center","align":"wide"} -->
<div class="wp-block-columns alignwide are-vertically-aligned-center">
<!-- wp:column {"verticalAlignment":"center","width":"60%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:60%">
<!-- wp:paragraph {"style":{"typography":{"lineHeight":"var(--wp--custom--line-height--medium)"}},"fontSize":"large"} -->
<p class="has-large-font-size" style="line-height:var(--wp--custom--line-height--medium)">Lorem ipsum dolor sit amet, consectetur adipiscing lectus. Vestibulum mi justo, luctus eu pellentesque vitae gravida non.</p>
<!-- /wp:paragraph -->
</div>
<!-- /wp:column -->
<!-- wp:column {"verticalAlignment":"center","width":"40%"} -->
<div class="wp-block-column is-vertically-aligned-center" style="flex-basis:40%">
<!-- wp:buttons {"layout":{"type":"flex","justifyContent":"right","orientation":"horizontal"}} -->
<div class="wp-block-buttons">
<!-- wp:button {"style":{"spacing":{"padding":{"top":"var(--wp--custom--spacing--sxs)","right":"var(--wp--custom--spacing--sm)","bottom":"var(--wp--custom--spacing--sxs)","left":"var(--wp--custom--spacing--sm)"}},"border":{"radius":"0px"}},"className":"is-style-fill"} -->
<div class="wp-block-button is-style-fill"><a class="wp-block-button__link" style="border-radius:0px;padding-top:var(--wp--custom--spacing--sxs);padding-right:var(--wp--custom--spacing--sm);padding-bottom:var(--wp--custom--spacing--sxs);padding-left:var(--wp--custom--spacing--sm)"><?php echo esc_html__( 'Let’s Get Started', 'wpct' ); ?> →</a></div>
<!-- /wp:button -->
</div>
<!-- /wp:buttons -->
</div>
<!-- /wp:column -->
</div>
<!-- /wp:columns -->
<!-- wp:spacer {"height":100} -->
<div style="height:100px" aria-hidden="true" class="wp-block-spacer"></div>
<!-- /wp:spacer -->
</div>
<!-- /wp:group -->
